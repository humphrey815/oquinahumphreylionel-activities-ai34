Chart.defaults.global.defaultFontColor = 'white';
var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MARCH', 'APRIL'],
        datasets: [{
            label: ['Borrowed Books'],
            data: [10, 6, 7, 5],
            borderColor:"black",
            backgroundColor: [
                '#33ccff',
                '#33ccff',
                '#33ccff',
                '#33ccff'
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Biology', 'Geography', 'History',],
        datasets: [{
            label: '# of Votes',
            data: [20, 40, 40,],
            borderColor:"black",
            backgroundColor: [
                'red',
                'yellow',
                'white',
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



