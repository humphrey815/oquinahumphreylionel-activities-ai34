import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../pages/dashboard.vue";
import Patron from "../pages/patron.vue";
import Book from "../pages/book.vue";
import Settings from "../pages/settings.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Dashboard",
      component: Dashboard,
    },
    {
      path: "/patron",
      name: "Patron",
      component: Patron,
    },

   {
      path: "/book",
      name: "Book",
      component: Book,
    },
    
    {
      path: "/settings",
      name: "Settings",
      component: Settings,
    },
    
  ],
});
