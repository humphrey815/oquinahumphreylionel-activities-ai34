<?php

namespace App\Http\Controllers;

use\App\Models\returnedbook;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Book = returnedbook::all();
        return response()->json($Book);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $BOOKS  = new returnedbook();

        $BOOKS->book_id = $request->book_id;
        $BOOKS->copies = $request->copies;
        $BOOKS->patron_id = $request->patron_id;

        $BOOKS->save();
        return response()->json($BOOKS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $BOOKS = returnedbook::find($id);
        return response()->json($BOOKS);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $BOOKS  = returnedbook::find($id);

        $BOOKS->book_id = $request->book_id;
        $BOOKS->copies = $request->copies;
        $BOOKS->patron_id = $request->patron_id;

        $BOOKS->update();
        return response()->json($BOOKS);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $BOOKS = returnedbook::find($id);
        $BOOKS->delete();
        return response()->json($BOOKS);
    }
}
