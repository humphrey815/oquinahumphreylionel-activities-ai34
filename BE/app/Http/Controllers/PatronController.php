<?php

namespace App\Http\Controllers;

use\App\Models\patron;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Book = patron::all();
        return response()->json($Book);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $BOOKS  = new patron();

        $BOOKS->last_name = $request->last_name;
        $BOOKS->first_name = $request->first_name;
        $BOOKS->middle_name = $request->middle_name;
        $BOOKS->email = $request->email;

        $BOOKS->save();
        return response()->json($BOOKS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $BOOKS = patron::find($id);
        return response()->json($BOOKS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $BOOKS  = patron::find($id);

        $BOOKS->last_name = $request->last_name;
        $BOOKS->first_name = $request->first_name;
        $BOOKS->middle_name = $request->middle_name;
        $BOOKS->email = $request->email;

        $BOOKS->update();
        return response()->json($BOOKS);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $BOOKS = patron::find($id);
        $BOOKS->delete();
        return response()->json($BOOKS);
    }
}
