<?php

namespace App\Http\Controllers;

use\App\Models\borrowedbook;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Book = borrowedbook::all();
        return response()->json($Book);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $BOOKS  = new borrowedbook();

        $BOOKS->patron_id = $request->patron_id;
        $BOOKS->copies = $request->copies;
        $BOOKS->book_id = $request->book_id;

        $BOOKS->save();
        return response()->json($BOOKS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $BOOKS = borrowedbook::find($id);
        return response()->json($BOOKS);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $BOOKS  = borrowedbook::find($id);

        $BOOKS->patron_id = $request->patron_id;
        $BOOKS->copies = $request->copies;
        $BOOKS->book_id = $request->book_id;

        $BOOKS->update();
        return response()->json($BOOKS);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $BOOKS = borrowedbook::find($id);
        $BOOKS->delete();
        return response()->json($BOOKS);
    }
}
