<?php

namespace Database\Seeders;

use App\Models\category;
use Illuminate\Database\Seeder;

class categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
    $mul_rows= [
    [ 'category' => 'Horror'],
    [ 'category' => 'Mystery'],
    [ 'category' => 'Mythology'],
    [ 'category' => 'Classic'],
    [ 'category' => 'Adventure']
];

    foreach ($mul_rows as $rows) {
    Category::create($rows);
        }
    }
}